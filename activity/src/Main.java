import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int num = 1;

        System.out.println("Input an integer whose factorial will be computed");

        try{
            num = in.nextInt();

        }catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }

        if (num <= 0) {
            System.out.println("You entered either a negative number or a zero value. Please try another one!");
        } else {

            long counterVal = 1;
            int initalVal = 1;

            //While loop solution
            while(initalVal < num) {
                initalVal++;
                counterVal *= initalVal;
            }
            //for loop solution
//                for (int i = initalVal; i <= num; i++) {
//                    counterVal *= i;
//                }

            System.out.println("The factorial of " + num + " is: " + counterVal);
        }


    }
}